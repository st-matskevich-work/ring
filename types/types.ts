export class User{
  username: string;
  socket: any;
  uid: number;
}

export class StreamCreateInfo {
  roomName: string;
  username: string;
  maxUsersNumber: number;
}

export class StreamCreateResponseInfo{
  uid: string;
  socket: any;
}

export class StreamConnectInfo{
  uid: string;
  username: string;
}

export class StreamConnectResponseInfo{
  uid: string;
  socket: any;
  uidConnected: User[];
}