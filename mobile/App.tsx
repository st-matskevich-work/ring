import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import StartScreen from './src/Screens/StartScreen'
import EnterStreamID from './src/Screens/EnterStreamID'
import CreateNewStream from './src/Screens/CreateNewStream'
import StreamScreen from './src/Screens/StreamScreen'
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { StackParamList } from 'src/LocalTypes/NavigationTypes'

declare const global: {HermesInternal: null | {}};

interface Props {
}

interface State {
  isLoggedIn: boolean
}


const Stack = createStackNavigator<StackParamList>();

export default class App extends React.Component<Props, State> {
  public state : State = {
    isLoggedIn: false
  };

  setLoggedIn = (status : boolean) : void => {
    this.setState({isLoggedIn: status});
  }
  
    render(){
      return (
        <NavigationContainer theme={DarkTheme}>
          <Stack.Navigator headerMode="none" screenOptions={{...TransitionPresets.SlideFromRightIOS}}>
            {
              !this.state.isLoggedIn ? <Stack.Screen name="Start" component={StartScreen}/> : null
            }
            {
              !this.state.isLoggedIn ? <Stack.Screen name="EnterStreamID"  initialParams={{setLoggedIn: this.setLoggedIn}} component={EnterStreamID}/> : null
            }
            {
              !this.state.isLoggedIn ? <Stack.Screen name="CreateNewStream" initialParams={{setLoggedIn: this.setLoggedIn}} component={CreateNewStream}/> : null
            }
            {
              this.state.isLoggedIn ? <Stack.Screen name="StreamScreen" initialParams={{setLoggedIn: this.setLoggedIn}} component={StreamScreen}/> : null
            }
          </Stack.Navigator>
        </NavigationContainer>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#23272a',
    flex: 1
  },
  workZone: {
    flex: 1,
  }
});
