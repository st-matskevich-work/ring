import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView
} from 'react-native';
import CustomButtom from '../Components/CustomButton'
import CustomHeader from '../Components/CustomHeader'
import { StackParamList } from '../LocalTypes/NavigationTypes'
import { StackNavigationProp } from '@react-navigation/stack';
import CustomTextInput from '../Components/CustomTextInput'

type CreateNewStreamScreenNavigationProp = StackNavigationProp<
  StackParamList,
  'CreateNewStream'
>;

interface Props {
    navigation: CreateNewStreamScreenNavigationProp,
    route: {
        params: {
            setLoggedIn: (status: boolean) => void;
        }
    }
}

interface State {
    streamCode: string
}

class CreateNewStream extends React.Component<Props, State>{
    joinStream = () : void => {
        this.props.route.params.setLoggedIn(true);
    }

    render(){
        return (
          <KeyboardAvoidingView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor="#23272a"/>
            <SafeAreaView style={styles.workZone}>
                <CustomHeader title="Create New Stream" onPress={() => this.props.navigation.navigate('Start')}/>
                <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
                    <View style={styles.labelContainer}>
                        <View style={styles.spacer}/>
                        <Image source={require('../Assets/logo.png')} style={styles.logoImage}/>
                        <Text style={styles.label}>Ring</Text>
                        <View style={styles.spacer}/>
                    </View>
                    <CustomTextInput placeholder="Room Name"/>
                    <View style={styles.inputSpacing}/>
                    <CustomTextInput placeholder="Description"/>
                    <View style={styles.inputSpacing}/>
                    <CustomTextInput placeholder="Maximum user amount"/>
                    <View style={styles.spacer}/>
                </ScrollView>
                <CustomButtom title="Create stream" onPress={this.joinStream}/>
            </SafeAreaView>
          </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#23272a',
        flex: 1,
        padding: 20
    },
    workZone: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: '#ffffff',
        fontSize: 48,
        fontFamily: 'UniSansHeavy'
    },
    labelContainer: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 60,
        alignItems: 'center',
        marginBottom: 60
    },
    logoImage: {
        height: 48,
        width: 48,
        //marginTop: -20,
        marginRight: 20
    },
    spacer: {
        flex: 1
    },
    inputSpacing: {
        margin: 10
    }
});

export default CreateNewStream