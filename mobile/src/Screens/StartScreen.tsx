import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image
} from 'react-native';
import CustomButtom from '../Components/CustomButton'
import { StackParamList } from '../LocalTypes/NavigationTypes'
import { StackNavigationProp } from '@react-navigation/stack';

type StartScreenNavigationProp = StackNavigationProp<
  StackParamList,
  'Start'
>;

interface Props {
    navigation: StartScreenNavigationProp
}

interface State {
}

class StartScreen extends React.Component<Props, State>{
    render(){
        return (
          <View style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor="#23272a"/>
            <SafeAreaView style={styles.workZone}>
                <View style={styles.spacer}/>
                <View style={styles.labelContainer}>
                    <Image source={require('../Assets/logo.png')} style={styles.logoImage}/>
                    <Text style={styles.label}>Ring</Text>
                    <View style={styles.spacer}/>
                </View>
                <View style={styles.spacer}/>
                <View style={styles.buttonBlock}>
                    <CustomButtom title="Connect to the stream" onPress={() => this.props.navigation.navigate('EnterStreamID')}/>
                    <View style={styles.labelContainer}>
                        <View style={styles.whiteLine}/>
                        <Text style={styles.buttonBreak}>or</Text>
                        <View style={styles.whiteLine}/>
                    </View>
                    <CustomButtom title="Start new stream" onPress={() => this.props.navigation.navigate('CreateNewStream')}/>
                </View>
                <View style={styles.spacer}/>
            </SafeAreaView>
          </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#23272a',
        flex: 1,
        padding: 20
    },
    workZone: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: '#ffffff',
        fontSize: 48,
        fontFamily: 'UniSansHeavy'
    },
    labelContainer: {
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        alignItems: 'center'
    },
    logoImage: {
        height: 48,
        width: 48,
        marginRight: 20
    },
    spacer: {
        flex: 1
    },
    buttonBlock: {
        width: '100%'
    },
    buttonBreak: {
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'center', 
        fontFamily: 'UniSansHeavy',
        //marginBottom: -5,
    },
    whiteLine: {
        flex: 1,
        height: 1,
        backgroundColor: '#ffffff',
        marginRight: 10,
        marginLeft: 10
    }
});

export default StartScreen;