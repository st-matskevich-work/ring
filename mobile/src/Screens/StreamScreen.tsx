import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  Button
} from 'react-native';
import CustomButtom from '../Components/CustomButton'
import CustomHeader from '../Components/CustomHeader'
import { StackParamList } from '../LocalTypes/NavigationTypes'
import { StackNavigationProp } from '@react-navigation/stack';
import CustomTextInput from '../Components/CustomTextInput'
import { NodeCameraView } from 'react-native-nodemediaclient';

type StreamScreenNavigationProp = StackNavigationProp<
  StackParamList,
  'StreamScreen'
>;

interface Props {
    navigation: StreamScreenNavigationProp,
    route: {
        params: {
            setLoggedIn: (status: boolean) => void;
        }
    }
}

interface State {
    streamCode: string
}

class StreamScreen extends React.Component<Props, State>{
    exit = () : void => {
        this.props.route.params.setLoggedIn(false);
    }

    render(){
        return (
          <KeyboardAvoidingView style={styles.container} behavior="padding">
            <StatusBar barStyle="light-content" backgroundColor="#23272a"/>
            <SafeAreaView style={styles.workZone}>
            <NodeCameraView 
              style={{ height: 400 }}
              ref={(vb) => { this.vb = vb }}
              outputUrl = {"rtmp://192.168.0.10/live/stream"}
              camera={{ cameraId: 1, cameraFrontMirror: true }}
              audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
              video={{ preset: 12, bitrate: 400000, profile: 1, fps: 15, videoFrontMirror: false }}
              autopreview={true}
            />
                <Button onPress={this.exit} title="Exit"/>
            </SafeAreaView>
          </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#23272a',
        flex: 1,
        padding: 20
    },
    workZone: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: '#ffffff',
        fontSize: 48,
        fontFamily: 'UniSansHeavyCAPS'
    },
    labelContainer: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 120,
        alignItems: 'center',
        marginBottom: 60
    },
    logoImage: {
        height: 48,
        width: 48,
        marginTop: -20,
        marginRight: 20
    },
    spacer: {
        flex: 1
    },
    inputSpacing: {
        margin: 10
    }
});

export default StreamScreen