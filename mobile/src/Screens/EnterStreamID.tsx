import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView
} from 'react-native';
import CustomButtom from '../Components/CustomButton'
import CustomHeader from '../Components/CustomHeader'
import CustomTextInput from '../Components/CustomTextInput'
import { StackParamList } from '../LocalTypes/NavigationTypes'
import { StackNavigationProp } from '@react-navigation/stack';

type EnterStreamIDScreenNavigationProp = StackNavigationProp<
  StackParamList,
  'EnterStreamID'
>;

interface Props {
    navigation: EnterStreamIDScreenNavigationProp,
    route: {
        params: {
            setLoggedIn: (status: boolean) => void;
        }
    }
}

interface State {
    streamCode: string
}

class EnterStreamID extends React.Component<Props, State>{
    joinStream = () : void => {
        this.props.route.params.setLoggedIn(true);
    }

    render(){
        return (
          <KeyboardAvoidingView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor="#23272a"/>
            <SafeAreaView style={styles.workZone}>
                <CustomHeader title="Enter Stream ID" onPress={() => this.props.navigation.navigate('Start')}/>
                <View style={styles.labelContainer}>
                    <View style={styles.spacer}/>
                    <Image source={require('../Assets/logo.png')} style={styles.logoImage}/>
                    <Text style={styles.label}>Ring</Text>
                    <View style={styles.spacer}/>
                </View>
                <View style={styles.spacer}/>
                <CustomTextInput placeholder="Stream id"/>
                <View style={styles.spacer}/>
                <View style={styles.spacer}/>
                <CustomButtom title="Connect" onPress={this.joinStream}/>
            </SafeAreaView>
          </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#23272a',
        flex: 1,
        padding: 20
    },
    workZone: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: '#ffffff',
        fontSize: 48,
        fontFamily: 'UniSansHeavy'
    },
    labelContainer: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 60,
        alignItems: 'center'
    },
    logoImage: {
        height: 48,
        width: 48,
        //marginTop: -20,
        marginRight: 20
    },
    spacer: {
        flex: 1
    }
});

export default EnterStreamID