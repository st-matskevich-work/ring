import React from 'react'
import {
    TextInput,
    StyleSheet,
    Animated,
    View,
    Easing,
    Keyboard
} from 'react-native'

interface Props {
    placeholder: string
}

interface State {
    text: string,
    inputWidth: number
}

export default class CustomTextInput extends React.Component<Props, State>{
    public indicatorColor : Animated.Value = new Animated.Value(0);
    public state : State = {
        text: "",
        inputWidth: 0
    };
    public textInput : TextInput | null = null

    componentDidMount(){
        //Keyboard.addListener('keyboardDidShow', this.onInputFocus);
        Keyboard.addListener('keyboardDidHide', this.onInputUnfocus);
    }

    componentWillUnmount(){
        //Keyboard.removeListener('keyboardDidShow', this.onInputFocus);
        Keyboard.removeListener('keyboaedDidHide', this.onInputUnfocus);
    }

    onInputFocus = () : void => {
        Animated.timing(
            this.indicatorColor,
            {
                toValue: 1000,
                duration: 500,
                easing: Easing.cubic,
                useNativeDriver: false
            }
        ).start()
    }

    onInputUnfocus = () : void => {
        Animated.timing(
            this.indicatorColor,
            {
                toValue: 0,
                duration: 500,
                easing: Easing.cubic,
                useNativeDriver: false
            }
        ).start()

        this.textInput?.blur();
    }

    render(){
        const interpolateWidth = this.indicatorColor.interpolate({
            inputRange: [0, 1000],
            outputRange: [0, this.state.inputWidth]
        })

        return(
            <>
                <TextInput style={styles.input}
                        ref={ref => this.textInput = ref}
                        onBlur={this.onInputUnfocus}
                        onFocus={this.onInputFocus}
                        onSubmitEditing={this.onInputUnfocus}
                        placeholderTextColor="#ffffff"
                        placeholder={this.props.placeholder}/>
                <View style={styles.indicatorHolder} onLayout={(event) => this.setState({inputWidth: event.nativeEvent.layout.width})}>
                    <View style={styles.indicator}/>
                    <Animated.View style={[styles.indicator, {width: interpolateWidth, position: 'absolute', backgroundColor: '#7289da'}]}/>
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        color: '#ffffff',
        width: '100%',
        fontFamily: 'UniSansHeavy',
        marginBottom: 5
    },
    indicatorHolder: {
        width: '100%',
        height: 1,
        flexDirection: 'row',
        marginBottom: 10,
        marginTop: -10
    },
    indicator: {
        height: '100%',
        width: '100%',
        backgroundColor: '#ffffff'
    }
})

