import React from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Image
} from 'react-native'

interface Props {
    title: string,
    onPress: () => void
}

interface State {
}

export default class CustomHeader extends React.Component<Props, State>{
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={this.props.onPress} style={styles.button}>
                    <Image source={require('../Assets/back.png')} style={styles.image}/>
                </TouchableOpacity>
                <View style={styles.spacer}/>
                <Text style={styles.label}>{this.props.title}</Text>
                <View style={styles.spacer}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 5,
        alignItems: 'center'
    },
    label: {
        color: '#ffffff',
        fontFamily: 'UniSansThin',
        fontSize: 16,
        marginTop: 5,
        alignSelf: 'center',
        textAlign: 'center'
    },
    image: {
        height: '100%',
        resizeMode: 'contain',
        width: '100%'
    },
    spacer: {
        flex: 1
    },
    button: {
        position: 'absolute',
        top: 2.5,
        width: 30,
        height: 30
    }
});