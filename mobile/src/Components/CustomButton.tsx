import React from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet
} from 'react-native'

interface Props {
    title: string,
    onPress: () => void
}

interface State {
}

export default class CustomButtom extends React.Component<Props, State>{
    render(){
        return(
            <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
                <Text style={styles.label}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#7289da',
        padding: 10,
        borderRadius: 30,
        marginBottom: 5,
        marginTop: 5
    },
    label: {
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: 'UniSansHeavy',
        //marginBottom: -5
    }
});