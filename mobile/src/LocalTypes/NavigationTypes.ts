import { StackNavigationProp } from '@react-navigation/stack';

export type StackParamList = {
    Start: undefined,
    EnterStreamID: { setLoggedIn: (status: boolean) => void },
    CreateNewStream: { setLoggedIn: (status: boolean) => void },
    StreamScreen: { setLoggedIn: (status: boolean) => void }
}
